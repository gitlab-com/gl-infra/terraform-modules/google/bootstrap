#!/bin/bash
# vim: ai:ts=8:sw=8:noet
#
# Bootstrap version: git
#

# Unregister the machine from Ubuntu Advantage
if type -P ua > /dev/null; then
  pro_command="ua"
elif type -P pro > /dev/null; then
  pro_command="pro"
fi

# We'll check for a "true" value in this file on startup to see if we should enroll in Pro.
$pro_command status --format json | jq -r .attached > /var/tmp/ubuntu_pro_enabled_on_shutdown || echo -n "false" > /var/tmp/ubuntu_pro_enabled_on_shutdown || true
$pro_command detach --assume-yes || true

CHEF_NODE_NAME="$(curl -s "http://metadata.google.internal/computeMetadata/v1/instance/attributes/CHEF_NODE_NAME" -H "Metadata-Flavor: Google")"
if [[ ! -f /etc/chef/client.pem ]]; then
    # No client.pem, nothing to do
    exit 0
fi

if type -P knife >/dev/null; then
    knife node delete "$CHEF_NODE_NAME" -c /etc/chef/client.rb -y
    knife client delete "$CHEF_NODE_NAME" -c /etc/chef/client.rb -y
fi

rm -f /etc/chef/client.pem
