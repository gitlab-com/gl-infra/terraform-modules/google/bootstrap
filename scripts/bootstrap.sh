#!/bin/bash
# vim: ai:ts=8:sw=8:noet
# This script is passed as a startup-script to GCP instances
#
# Bootstrap version: git
#

###################################################
###    NOTE: It is being run on _every_ boot    ###
###  It MUST be non destructive and idempotent  ###
###################################################

exec &> >(tee -a "/var/tmp/bootstrap-$(date +%Y%m%d-%H%M%S).log")
set -xeu

SECONDS=0
echo "$(date -u): Bootstrap start"

## If chef was disabled via shim, set a variable.
chef_enabled=true
if [[ -f /usr/local/bin/chef-client-is-enabled ]]; then
  # Chef client shim is installed
  if ! /usr/local/bin/chef-client-is-enabled; then
    # Chef is disabled
    chef_enabled=false
  fi
fi

# needrestart is included in Ubuntu 22.04+, which defaults to interactive mode when prompting
# to restart services. Override this to automatically restart necessary services.
export NEEDRESTART_MODE='a'

env

cleanup () {
  duration=$SECONDS
  if [[ "${1:-0}" != "0" ]]; then
    echo "$(date -u): Bootstrap crashed in $(($duration / 60)) minutes and $(($duration % 60)) seconds"
    exit 1
  fi

  # If there was a package installed in the initial bootstrap, that requires a reboot, do it.
  # Subsequent runs after the initial one, will not trigger this reboot.
  if [[ ! -f /var/tmp/inital-boot-run.lock ]] && [[ -f /var/run/reboot-required ]] ; then
    touch /tmp/bootstrap-reboot
  fi

  # If GL_INITIAL_BOOT_FORCE_REBOOT=true, we will always force a reboot after the initial boot.
  # This is useful for re-provisioning machines, since the re-mounting logic can leave some
  # parts of the system (e.g. /var/log) in an odd state. An extra post-provision reboot leaves
  # the system in a more predictable state.
  if [[ ! -f /var/tmp/inital-boot-run.lock ]] && [[ "${GL_INITIAL_BOOT_FORCE_REBOOT:-false}" == "true" ]] ; then
    touch /tmp/bootstrap-reboot
  fi

  touch /var/tmp/inital-boot-run.lock

  if [[ -f /tmp/bootstrap-reboot ]]; then
    rm -f /tmp/bootstrap-reboot
    reboot
    exit 0
  fi

  echo "$(date -u): Bootstrap finished in $(($duration / 60)) minutes and $(($duration % 60)) seconds"
}

trap 'cleanup $?' ERR

# Pass env variables
for i in $(curl -s "http://metadata.google.internal/computeMetadata/v1/instance/attributes/" -H "Metadata-Flavor: Google"); do
    if [[ $i == CHEF* ]]; then
        export "$i"="$(curl -s "http://metadata.google.internal/computeMetadata/v1/instance/attributes/$i" -H "Metadata-Flavor: Google")"
    fi
    if [[ $i == GL* ]]; then
        export "$i"="$(curl -s "http://metadata.google.internal/computeMetadata/v1/instance/attributes/$i" -H "Metadata-Flavor: Google")"
    fi
done

pgbouncer_cleanup() {
    # Clean up residual pgbouncer PID files
    # https://gitlab.com/gitlab-com/gl-infra/production/-/issues/7932
    # Make sure any residual INI files are cleaned up as well
    # https://gitlab.com/gitlab-com/gl-infra/production/-/issues/17550
    if [[ -d /var/opt/gitlab/pgbouncer ]]; then
        # rather than leave stale PID files and risking PIDs matching new processes
        # post-reboot, just remove the PID files so they can be re-created properly.
        rm /var/opt/gitlab/pgbouncer/*.pid || true
        for i in $( find /var/opt/gitlab/pgbouncer -name pgbouncer*.ini ); do
            local iowner="$( stat -c "%U" $i )"
            if [[ "$iowner" != "gitlab-psql" ]]; then
                echo "Removing stale/incorrect INI file: $i"
                rm $i
            fi
        done
    fi
}

format_ext4() {
    mkfs.ext4 -m 0 -F -E lazy_itable_init=0,lazy_journal_init=0,discard "$1"
}

mount_device() {
    local device_path="$1"
    local mount_path="$2"
    local UUID="$(sudo blkid -s UUID -o value $device_path)"

    mkdir -p "$mount_path"

    # Use \s delimiters around $mount_path.  This ensures we don't remove a nested mount point
    # or prefixed mountpoint accidentally from /etc/fstab, causing issues post-reboot.
    if grep -qs "\s$mount_path\s" /etc/fstab && ! grep -qs "$UUID" /etc/fstab; then
        # We found our mount path referencing the wrong disk. Remove it from fstab.
        cp /etc/fstab "/var/tmp/fstab.bak.$(date +"%s.%6N")"
        sed -i "/${mount_path//\//\\/}/d" /etc/fstab
        systemctl daemon-reload
    fi

    if ! grep -qs "$UUID" /etc/fstab ; then
        echo UUID="$UUID" "$mount_path" ext4 discard,defaults 0 2 | tee -a /etc/fstab
    fi

    if ! grep -qs "\s$mount_path\s" /proc/mounts; then
        mount -o discard,defaults $device_path "$mount_path"
    fi
}

if [[ -L /dev/disk/by-id/google-log ]]; then
    if [[ $(file -sL /dev/disk/by-id/google-log) != *Linux* ]]; then
      format_ext4 /dev/disk/by-id/google-log
    fi

    # In case we resized the underlying GCP disk, this might fail in some circumstances though.
    resize2fs /dev/disk/by-id/google-log || true

    mount_device /dev/disk/by-id/google-log /var/log
fi

# Set to false to prevent this script from formatting and mounting data disks.
# In the long run, it may be sensible to cut over to gitlab_disks for this
# functionality (https://gitlab.com/gitlab-cookbooks/gitlab_disks), including
# log disks. At the time of writing, we intend to roll out gitlab_disks (and set
# this var to false) for only one class of node: git storage nodes that will use
# ZFS, and so we don't want the bootstrap script interfering with their disks.
GL_BOOTSTRAP_DATA_DISK="${GL_BOOTSTRAP_DATA_DISK:-true}"

# default to false, force a reformat even if there is an existing
# Linux filesystem
GL_FORMAT_DATA_DISK="${GL_FORMAT_DATA_DISK:-false}"

# same as above, only for LVM-based data disks.
GL_BOOTSTRAP_LVM_DATA_DISK="${GL_BOOTSTRAP_LVM_DATA_DISK:-false}"
GL_FORMAT_LVM_DATA_DISK="${GL_FORMAT_LVM_DATA_DISK:-false}"
GL_STRIPED_LVM_DATA_DISK="${GL_STRIPED_LVM_DATA_DISK:-false}"
GL_LVM_DATA_DISK_COUNT="${GL_LVM_DATA_DISK_COUNT:-0}"
GL_LVM_EXTENT_SIZE="${GL_LVM_EXTENT_SIZE:-4M}"

if [[ -b /dev/disk/by-id/google-data ]]; then
  DATA_DEVICE="/dev/disk/by-id/google-data"
else
  DATA_DEVICE="/dev/disk/by-id/google-persistent-disk-1"
fi
echo "DATA_DEVICE=${DATA_DEVICE}"

if [[ -b /dev/mapper/gitlab--vg-gitlab--data ]]; then
  LVM_DATA_DEVICE="/dev/mapper/gitlab--vg-gitlab--data"
else
  LVM_DATA_DEVICE=""
fi
echo "LVM_DATA_DEVICE=${LVM_DATA_DEVICE:-}"

if [ "$GL_BOOTSTRAP_DATA_DISK" = "true" ]; then
    if [[ -b ${DATA_DEVICE} && ("true" ==  "${GL_FORMAT_DATA_DISK}" || $(file -sL ${DATA_DEVICE}) != *Linux*) ]]; then
        format_ext4 ${DATA_DEVICE}
    fi

    # Proceed with mounting
    if [[ -L ${DATA_DEVICE} ]]; then
        mount_device ${DATA_DEVICE} "${GL_PERSISTENT_DISK_PATH:-/var/opt/gitlab}"
    fi
fi

if [[ -L /dev/disk/by-id/google-wal ]]; then
    if [[ $(file -sL /dev/disk/by-id/google-wal) != *Linux* ]]; then
      format_ext4 /dev/disk/by-id/google-wal
    fi

    # In case we resized the underlying GCP disk, this might fail in some circumstances though.
    resize2fs /dev/disk/by-id/google-wal || true

    mount_device /dev/disk/by-id/google-wal /var/opt/gitlab-wal
fi


if [[ "$GL_BOOTSTRAP_LVM_DATA_DISK" == "true" && $GL_LVM_DATA_DISK_COUNT -gt 0 ]]; then
  # sanity check on whether or not LVM DATA DEVICE has been previously constructed.
  # if it has, skip this step.
  if [[ ! -n ${LVM_DATA_DEVICE:-} ]]; then
    ZERO_INDEXED_DISK_COUNT=$(( $GL_LVM_DATA_DISK_COUNT - 1 ))
    PV_LIST=""
    for i in $(seq 0 $ZERO_INDEXED_DISK_COUNT); do
      pvcreate -y -ff /dev/disk/by-id/google-lvm-${i}; # we're (re)bootstrapping the lvm data disk, so force.
      PV_LIST="$PV_LIST /dev/disk/by-id/google-lvm-${i}"
    done

    vgcreate -s ${GL_LVM_EXTENT_SIZE} gitlab-vg ${PV_LIST}

    if [[ $GL_STRIPED_LVM_DATA_DISK == "true" ]]; then
      # -i is how many devices to stripe across
      # -I is how large stripes should be - match the PE size
      lvcreate -i ${GL_LVM_DATA_DISK_COUNT} -I ${GL_LVM_EXTENT_SIZE} -l 100%FREE -n gitlab-data gitlab-vg
    else
      # create a plain linear LVM configuration
      lvcreate -l 100%FREE -n gitlab-data gitlab-vg
    fi

    # need to ensure this is set on first run for the ensuing format
    LVM_DATA_DEVICE="/dev/mapper/gitlab--vg-gitlab--data"
  fi
  
  if [[ -b ${LVM_DATA_DEVICE} && ("true" ==  "${GL_FORMAT_LVM_DATA_DISK}" || $(file -sL ${LVM_DATA_DEVICE}) != *Linux*) ]]; then
    format_ext4 ${LVM_DATA_DEVICE}
  fi

  # Proceed with mounting
  if [[ -L ${LVM_DATA_DEVICE} ]]; then
    mount_device ${LVM_DATA_DEVICE} "${GL_PERSISTENT_LVM_DISK_PATH:-/var/opt/gitlab-lvm}"
  fi
fi

# This file *should* exist on all recent distros. But let's not rely on that.
# See: https://www.freedesktop.org/software/systemd/man/os-release.html
source /etc/os-release || true

##
# enroll_ubuntu_advantage
#
# This will enroll Ubuntu Advantage with the token that is provided via
# $GL_UBUNTU_ADVANTAGE_TOKEN.
#
# If $GL_SKIP_UBUNTU_ADVANTAGE is true or we are not running on
# ubuntu, this will be skipped.
enroll_ubuntu_advantage () {
  if [[ "${GL_SKIP_UBUNTU_ADVANTAGE:-false}" == "true" ]]; then
    echo "GL_SKIP_UBUNTU_ADVANTAGE is true, skipping Ubuntu Advantage enrollment"
    return
  fi

  # We're going to let Chef be in full control of whether a node is enrolled in Pro or not.
  # But we want to ensure we preserve enrollment state if Chef is disabled when rebooted.
  # This file is written by teardown.sh before unenrollment to indicate what the state was.
  should_enroll="$(cat /var/tmp/ubuntu_pro_enabled_on_shutdown 2>/dev/null || echo "false")"
  if [[ "$should_enroll" != "true" ]]; then
    return
  fi

  # from os-release
  if [[ "${ID:-}" == "ubuntu" ]]; then
    ## Wait for APT to be available
    while sudo fuser /var/{lib/{dpkg,apt/lists},cache/apt/archives}/lock >/dev/null 2>&1; do
      echo "Waiting until the package manager is free..."
      sleep 2
    done

    apt-get update
    apt-get install -y jq ubuntu-advantage-tools

    # Grabbing the token from Vault.
    GL_UBUNTU_ADVANTAGE_TOKEN="$(retrieve_secret_from_vault "shared/ubuntu-advantage" "enrollment_key" || true )"

    if [[ -z "${GL_UBUNTU_ADVANTAGE_TOKEN:-}" ]]; then
      echo "GL_UBUNTU_ADVANTAGE_TOKEN is empty, skipping Ubuntu Advantage enrollment"
      return
    fi

    /usr/bin/ua attach "${GL_UBUNTU_ADVANTAGE_TOKEN:-}" || true
    /usr/bin/ua enable esm-infra || true
  else
    echo "We are not running on Ubuntu, skipping Ubuntu Advantage enrollment"
    return
  fi
}

retrieve_secret_from_vault () {
  if [[ -z "$2" ]]; then
    >&2 echo "ERROR: usage: retrieve_secret_from_vault <secret path> <secret key>"
    >&2 echo "Returning an empty string."
    echo -n ""
    return
  fi

  SECRET_PATH="$1"
  SECRET_KEY="$2"
  VAULT_ADDR="${VAULT_ADDR:-https://vault.ops.gke.gitlab.net}"

  VAULT_ROLE="chef_${VAULT_ENV:-$CHEF_ENVIRONMENT}_base"
  VAULT_AUD="${VAULT_ADDR}/vault/${VAULT_ROLE}"
  JWT_TOKEN="$(curl -s --fail --header "Metadata-Flavor: Google" --get --data-urlencode "audience=${VAULT_AUD}" --data-urlencode "format=full" "http://metadata/computeMetadata/v1/instance/service-accounts/default/identity")"
  VAULT_TOKEN="$(curl -s --fail -X POST -d "{\"role\": \"$VAULT_ROLE\", \"jwt\": \"$JWT_TOKEN\"}" "$VAULT_ADDR/v1/auth/gcp/login" | jq -r '.auth.client_token')"

  SECRET="$(curl -s --fail -H "X-Vault-Token: $VAULT_TOKEN" "$VAULT_ADDR/v1/chef/data/$SECRET_PATH")"

  jq -r --arg KEY "$SECRET_KEY" '.data.data[$KEY]' <<< "$SECRET"
}

install_chef_client() {
	curl -L https://omnitruck.chef.io/install.sh | sudo bash -s -- -v "${CHEF_VERSION}"
	# Holding chef is important, as we need to use specific versions.
	apt-mark hold chef

	# Symlink the file used by Chef as the CA trust store to the one provided by the "ca-certificates"
	# package.
	# This is particularly useful for talking to endpoints protected by Let's Encrypt certs
	# that would otherwise fail to verify using the chef root certs file. If you're wondering why
	# we do this here and not inside Chef, it's because our cookbooks are written in a way that they run
	# a lot of Ruby code during compile time and if they need to talk to an endpoint protected by
	# a Let's Encrypt cert, then the certificate verification will fail unless this symlink is in place.
	ln -sf /etc/ssl/certs/ca-certificates.crt /opt/chef/embedded/ssl/cert.pem

	mkdir -p /etc/chef

	# create client.rb
	cat >/etc/chef/client.rb <<-EOF
chef_server_url  "$CHEF_URL"
validation_client_name "gitlab-validator"
log_location   STDOUT
node_name "$CHEF_NODE_NAME"
environment "$CHEF_ENVIRONMENT"
	EOF
}

# Install the Cinc distribution of chef-client, hook up some symlinks to maintain backwards compatibility with Chef.
install_cinc_client() {
	curl -L https://omnitruck.cinc.sh/install.sh | sudo bash -s -- -v "$CHEF_VERSION"

	apt-mark hold cinc

	mkdir -p /etc/cinc

	if [[ ! -d /etc/chef ]]; then
		ln -s /etc/cinc /etc/chef
	fi

	if [[ ! -d /opt/chef ]]; then
		ln -s /opt/cinc /opt/chef
	fi

	ln -sf /etc/ssl/certs/ca-certificates.crt /opt/cinc/embedded/ssl/cert.pem
	# create client.rb
	cat >/etc/chef/client.rb <<-EOF
chef_server_url  "$CHEF_URL"
validation_client_name "gitlab-validator"
log_location   STDOUT
node_name "$CHEF_NODE_NAME"
environment "$CHEF_ENVIRONMENT"
ohai.optional_plugins = [
  :Passwd
]
	EOF
}

# Install chef if it is not already installed, and bootstrap a client.
install_chef () {
  if ! node_is_registered_in_chef; then
    # We need build-essential in order to bootstrap recipes depending on rexml
    # See:
    # - https://github.com/ruby/rexml/issues/131
    # - https://gitlab.com/gitlab-com/gl-infra/production-engineering/-/issues/25426
    apt-get update
    apt-get install -y build-essential

    CHEF_MAJOR_VERSION="$(awk -F'.' '{print $1}' <<<"$CHEF_VERSION")"
    if [[ "${ID:-}" == "ubuntu" ]]; then
      OS_MAJOR_VERSION="$(lsb_release -r -s | awk -F'.' '{print $1}')"
    else
      OS_MAJOR_VERSION=""
    fi

    if [[ "$OS_MAJOR_VERSION" -gt 20 ]] && [[ "$CHEF_MAJOR_VERSION" -lt 16 ]]; then
      # If we're provisioning an Ubuntu 22+ node, pin Chef version to _at least_ 16.
      CHEF_VERSION=16.18.30
      CHEF_MAJOR_VERSION=16
    fi

    if [[ "$CHEF_MAJOR_VERSION" -gt 14 ]]; then
        install_cinc_client
    else
        install_chef_client
    fi

    if [[ -e /etc/chef/client.pem ]]; then
      # handle the case where the node was removed from Chef, but we didn't didn't
      # cleanup the credential, we'll attempt to re-register. preserve the old key in the event of
      # a transient failure in detecting registration state.
      mv "/etc/chef/client.pem" "/etc/chef/client.pem.bak-$(date +%s)"
    fi

    # If we skip the kernel install, then there might be a case where we don't
    # run a supported kernel. So we do not override the attributes here.
    if [[ "${GL_SKIP_KERNEL_INSTALL:-false}" == "true" ]]; then
      cat > /dev/shm/attributes.json <<-EOF
{
  "gitlab-server": {
    "linux-gcp": []
  }
}
EOF
    else
      # Force letting this bootup script handle the kernel image
      cat > /dev/shm/attributes.json <<-EOF
{
  "gitlab-server": {
    "linux-gcp": [],
    "ubuntu-advantage": {
      "enroll": true,
      "esm-infra": true,
      "livepatch": true
    }
  }
}
EOF
    fi

    # The omnibus version is managed via deployer by updating the $env-omnibus-version
    # role in chef directly.
    #
    # During a packer build, we want the ability to override the version for a
    # particular packer run. We cannot use node attribute overrides, because
    # those get lost during a reboot.
    if [[ "${GL_OMNIBUS_PACKAGE_VERSION:-}" != "" ]]; then
      sed '$ d' /dev/shm/attributes.json > /dev/shm/attributes.json.tmp
      cat >> /dev/shm/attributes.json.tmp <<-EOF
  ,
  "omnibus-gitlab": {
    "package": {
      "version": "$GL_OMNIBUS_PACKAGE_VERSION"
    }
  }
}
EOF
      mv /dev/shm/attributes.json.tmp /dev/shm/attributes.json
    fi

    # If this node has an overridden service account set up for root, this can break provisioning.
    if [[ -s /root/.config/gcloud/configurations/config_default ]]; then
      cp /root/.config/gcloud/configurations/config_default "/var/tmp/gcp_config_default.bak.$(date +%s)"
      echo > /root/.config/gcloud/configurations/config_default
    fi

    # Get validation.pem from gkms and register node
    gsutil cp "gs://$CHEF_BOOTSTRAP_BUCKET/validation.enc" /tmp/validation.enc
    gcloud_opts=()
    if [[ -n "$CHEF_PROJECT" ]]; then
      gcloud_opts+=("--project=$CHEF_PROJECT")
    fi
    gcloud_opts+=(
      "kms" "decrypt"
      "--keyring=$CHEF_BOOTSTRAP_KEYRING"
      "--location=global"
      "--key=$CHEF_BOOTSTRAP_KEY"
      "--plaintext-file=/etc/chef/validation.pem"
      "--ciphertext-file=/tmp/validation.enc"
    )
    gcloud "${gcloud_opts[@]}"

    # register client, as per chef's documentation, the initial run is always
    # performed with an empty run-list.
    # The actual chef-run with all the cookbooks is handled in `run_chef`
    chef-client -j /dev/shm/attributes.json
    rm -f /tmp/validation.enc /etc/chef/validation.pem
  fi
}

# Run chef with the run-list specified in the node's metadata.
run_chef () {
  # persist the run list
  knife node -c /etc/chef/client.rb run_list set "$CHEF_NODE_NAME" $(echo $CHEF_RUN_LIST | tr -d '"')

  # If we know that we're coming online from a VM snapshot, do some cleanup.
  if [[ -f /var/tmp/pre-snap-tempfile ]]; then
    # Clear this apt source, otherwise we may not install the gitlab-ee packages correctly
    if [[ -f /etc/apt/sources.list.d/gitlab_pre-release.list ]]; then
      rm -rf "/etc/apt/sources.list.d/gitlab_pre-release.list"
    fi

    # Allow Consul to reset it's ID
    if [[ -f /var/lib/consul/node-id ]]; then
      rm -rf /var/lib/consul/node-id
    fi

    # Enable chef-client if it was disabled when the OS disk snapshot was taken.
    if [[ -x /usr/local/bin/chef-client-enable ]] && [[ "$chef_enabled" == false ]]; then
      /usr/local/bin/chef-client-enable
      chef_enabled=true
    fi
  fi

  if [[ "$chef_enabled" == false ]]; then
    # If this var is set, we know that the node was previously bootstrapped, and intentionally disabled.
    # We should still allow the node to re-register and set it's own runlist, but preserve the disabled state
    # of chef-client itself
    if [[ -f /usr/local/bin/chef-client-disable ]]; then
      /usr/local/bin/chef-client-disable "Disabled by GCP startup-script to preserve previously disabled state."
    else
      systemctl stop chef-client
      systemctl disable chef-client
    fi
  else
    # run chef using the new or modified runlist
    chef-client -j /dev/shm/attributes.json

    # On first boot run the additional runlist if it is defined.
    if [[ ! -e /var/tmp/inital-boot-run.lock && -n "${CHEF_INIT_RUN_LIST:-}" ]]; then
      CHEF_CLIENT_ARGS="-o $(echo "$CHEF_INIT_RUN_LIST" | sed 's/"\|,$//g')"
      chef-client -j /dev/shm/attributes.json $CHEF_CLIENT_ARGS
    fi
  fi
}

node_is_registered_in_chef() {
  # This serves two purposes, one validating that the client credentials work, and two that the
  # node exists w/ the correct name in the server.
  [[ -f /etc/chef/client.pem ]] && [[ -f /etc/chef/client.rb ]] && knife node -c /etc/chef/client.rb show "$CHEF_NODE_NAME"
}

post_restore_ops() {
  # restart any services that were disabled prior to snapshot.
  if [[ -f /etc/google/snapshots/systemd-services-to-disable ]]; then
    all_services="$(systemctl list-unit-files --type=service)"

    while read systemd_unit; do
      if grep "$systemd_unit" > /dev/null <<< "$all_services"; then
        systemctl enable "$systemd_unit"
        systemctl restart "$systemd_unit"
      fi
    done < /etc/google/snapshots/systemd-services-to-disable
  fi

  rm -rf /var/tmp/pre-snap-tempfile
  # Force a reboot of the machine onto newly mounted disks
  export GL_INITIAL_BOOT_FORCE_REBOOT=true
  rm -rf /var/tmp/inital-boot-run.lock
}

pgbouncer_cleanup
install_chef
enroll_ubuntu_advantage
run_chef
if [[ -f /var/tmp/pre-snap-tempfile ]]; then
  post_restore_ops
fi
cleanup 0

# Kick off an unattended-upgrade just in case. We can only do this after chef
# has written our configuration. As per our chef config, this will only install
# security updates.
unattended-upgrade
