output "bootstrap" {
  value = file("${path.module}/scripts/bootstrap.sh")
}

output "teardown" {
  value = file("${path.module}/scripts/teardown.sh")
}
