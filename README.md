# GitLab.com Bootstrap Terraform Module

## What is this?

Bootstrap script for GCE instances.

## Usage

This module will return bootstrap and teardown scripts for a GCE VM as module outputs.

```hcl
module "bootstrap" {
  source  = "ops.gitlab.net/gitlab-com/bootstrap/google"
  version = "5.0.0"
}
```

## What is Terraform?

[Terraform](https://www.terraform.io) is an infrastructure-as-code tool that greatly reduces the amount of time needed to implement and scale our infrastructure. It's provider agnostic so it works great for our use case. You're encouraged to read the documentation as it's very well written.

## Caveats

When specifying mount points, care should be taken not to nest mount points within each other, or to at least ensure correct ordering when nesting mount points.  Best practice would be to utilize non-nested mount points and symlink to the final destination.  See scripts/bootstrap.sh mount_device() for more details.

<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 1.0 |

## Providers

No providers.

## Modules

No modules.

## Resources

No resources.

## Inputs

No inputs.

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_bootstrap"></a> [bootstrap](#output\_bootstrap) | n/a |
| <a name="output_teardown"></a> [teardown](#output\_teardown) | n/a |
<!-- END_TF_DOCS -->

## Contributing

Please see [CONTRIBUTING.md](./CONTRIBUTING.md).

## License

See [LICENSE](./LICENSE).
